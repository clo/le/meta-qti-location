# location packages which are common across various machines
IMAGE_INSTALL += "loc-hal"
IMAGE_INSTALL += "location-api"
IMAGE_INSTALL += "gps-utils"
IMAGE_INSTALL += "location-hal-daemon"
IMAGE_INSTALL += "location-client-api"
IMAGE_INSTALL += "loc-socket"
IMAGE_INSTALL += "location-client-api-testapp"
IMAGE_INSTALL += "location-integration-api"
