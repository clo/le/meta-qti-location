inherit autotools-brokensep pkgconfig
require ${COREBASE}/meta-qti-location/recipes/include/common-location-defines.inc
require ${COREBASE}/meta-qti-location/recipes/include/location-license-bsd3-clause.inc

DESCRIPTION = "location api msg protobuf library"

FILESPATH =+ "${WORKSPACE}:"
SRC_URI = "file://qcom-opensource/location/location_api_msg_proto/"
S = "${WORKDIR}/qcom-opensource/location/location_api_msg_proto"

DEPENDS = "glib-2.0 protobuf protobuf-native loc-pla-hdr gps-utils"

FILES_${PN} += "${libdir}/*"

do_compile_prepend () {
    echo "Running location_api_msg_protobuf_gen.sh"
    cd ${S}
    ./location_api_msg_protobuf_gen.sh
    cd -
}
